// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'riverpod_providers.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$helloHash() => r'ffbefcd5476c99df1d11a880cf5f673e1139abc8';

/// See also [hello].
@ProviderFor(hello)
final helloProvider = AutoDisposeProvider<String>.internal(
  hello,
  name: r'helloProvider',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$helloHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef HelloRef = AutoDisposeProviderRef<String>;
String _$greetingHash() => r'dafd10a54244d43332d4937c6450b8d86ef36d58';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [greeting].
@ProviderFor(greeting)
const greetingProvider = GreetingFamily();

/// See also [greeting].
class GreetingFamily extends Family<String> {
  /// See also [greeting].
  const GreetingFamily();

  /// See also [greeting].
  GreetingProvider call(
    String otherMessage,
  ) {
    return GreetingProvider(
      otherMessage,
    );
  }

  @override
  GreetingProvider getProviderOverride(
    covariant GreetingProvider provider,
  ) {
    return call(
      provider.otherMessage,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'greetingProvider';
}

/// See also [greeting].
class GreetingProvider extends AutoDisposeProvider<String> {
  /// See also [greeting].
  GreetingProvider(
    String otherMessage,
  ) : this._internal(
          (ref) => greeting(
            ref as GreetingRef,
            otherMessage,
          ),
          from: greetingProvider,
          name: r'greetingProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$greetingHash,
          dependencies: GreetingFamily._dependencies,
          allTransitiveDependencies: GreetingFamily._allTransitiveDependencies,
          otherMessage: otherMessage,
        );

  GreetingProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.otherMessage,
  }) : super.internal();

  final String otherMessage;

  @override
  Override overrideWith(
    String Function(GreetingRef provider) create,
  ) {
    return ProviderOverride(
      origin: this,
      override: GreetingProvider._internal(
        (ref) => create(ref as GreetingRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        otherMessage: otherMessage,
      ),
    );
  }

  @override
  AutoDisposeProviderElement<String> createElement() {
    return _GreetingProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is GreetingProvider && other.otherMessage == otherMessage;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, otherMessage.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin GreetingRef on AutoDisposeProviderRef<String> {
  /// The parameter `otherMessage` of this provider.
  String get otherMessage;
}

class _GreetingProviderElement extends AutoDisposeProviderElement<String>
    with GreetingRef {
  _GreetingProviderElement(super.provider);

  @override
  String get otherMessage => (origin as GreetingProvider).otherMessage;
}

String _$helloAtHash() => r'aaf44863329d7009e39eaf4e12f6be750fa0e39d';

abstract class _$HelloAt extends BuildlessAutoDisposeNotifier<String> {
  late final String othersName;

  String build(
    String othersName,
  );
}

/// See also [HelloAt].
@ProviderFor(HelloAt)
const helloAtProvider = HelloAtFamily();

/// See also [HelloAt].
class HelloAtFamily extends Family<String> {
  /// See also [HelloAt].
  const HelloAtFamily();

  /// See also [HelloAt].
  HelloAtProvider call(
    String othersName,
  ) {
    return HelloAtProvider(
      othersName,
    );
  }

  @override
  HelloAtProvider getProviderOverride(
    covariant HelloAtProvider provider,
  ) {
    return call(
      provider.othersName,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'helloAtProvider';
}

/// See also [HelloAt].
class HelloAtProvider extends AutoDisposeNotifierProviderImpl<HelloAt, String> {
  /// See also [HelloAt].
  HelloAtProvider(
    String othersName,
  ) : this._internal(
          () => HelloAt()..othersName = othersName,
          from: helloAtProvider,
          name: r'helloAtProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$helloAtHash,
          dependencies: HelloAtFamily._dependencies,
          allTransitiveDependencies: HelloAtFamily._allTransitiveDependencies,
          othersName: othersName,
        );

  HelloAtProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.othersName,
  }) : super.internal();

  final String othersName;

  @override
  String runNotifierBuild(
    covariant HelloAt notifier,
  ) {
    return notifier.build(
      othersName,
    );
  }

  @override
  Override overrideWith(HelloAt Function() create) {
    return ProviderOverride(
      origin: this,
      override: HelloAtProvider._internal(
        () => create()..othersName = othersName,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        othersName: othersName,
      ),
    );
  }

  @override
  AutoDisposeNotifierProviderElement<HelloAt, String> createElement() {
    return _HelloAtProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is HelloAtProvider && other.othersName == othersName;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, othersName.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin HelloAtRef on AutoDisposeNotifierProviderRef<String> {
  /// The parameter `othersName` of this provider.
  String get othersName;
}

class _HelloAtProviderElement
    extends AutoDisposeNotifierProviderElement<HelloAt, String>
    with HelloAtRef {
  _HelloAtProviderElement(super.provider);

  @override
  String get othersName => (origin as HelloAtProvider).othersName;
}

String _$seenNumbersHash() => r'a48977ed58aa851f7b730adb264947dd00591afa';

/// See also [SeenNumbers].
@ProviderFor(SeenNumbers)
final seenNumbersProvider =
    AutoDisposeNotifierProvider<SeenNumbers, List<num>>.internal(
  SeenNumbers.new,
  name: r'seenNumbersProvider',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$seenNumbersHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$SeenNumbers = AutoDisposeNotifier<List<num>>;
String _$timestampHash() => r'eebedc31583c573cad515bac541aac49a2442d88';

/// See also [Timestamp].
@ProviderFor(Timestamp)
final timestampProvider =
    AutoDisposeNotifierProvider<Timestamp, TimestampWrapper>.internal(
  Timestamp.new,
  name: r'timestampProvider',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$timestampHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$Timestamp = AutoDisposeNotifier<TimestampWrapper>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
