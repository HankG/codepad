import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'riverpod_providers.dart';

void main() {
  runApp(
    const ProviderScope(
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Riverpod Lifecycle Demo',
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends ConsumerWidget {
  const MyHomePage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    debugPrint('[MyHomePage] Build');
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(ref.watch(helloProvider)),
            Text(ref.watch(helloProvider)),
            Text(ref.watch(greetingProvider('How are you today?'))),
            Text(ref.watch(greetingProvider('How are you today?'))),
            Text(ref.watch(greetingProvider('What time is it?'))),
            Text(ref.watch(greetingProvider('What time is it?'))),
            Text(ref.watch(helloAtProvider('Terry'))),
            Text(ref.watch(helloAtProvider('Terry'))),
            Text(ref.watch(helloAtProvider('Pat'))),
            Text(ref.watch(helloAtProvider('Pat'))),
            Text(ref.watch(seenNumbersProvider).toString()),
            Text(ref.watch(timestampProvider).dateTime.toString())
          ],
        ),
      ),
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          FloatingActionButton(
            child: const Text('T'),
            onPressed: () {
              ref.read(helloAtProvider('Terry').notifier).refresh();
            },
          ),
          FloatingActionButton(
            child: const Text('P'),
            onPressed: () {
              ref.read(helloAtProvider('Pat').notifier).refresh();
            },
          ),
          FloatingActionButton(
            child: const Text('N+'),
            onPressed: () {
              ref.read(seenNumbersProvider.notifier).addNumberNewState();
            },
          ),
          FloatingActionButton(
            child: const Text('I+'),
            onPressed: () {
              ref.read(seenNumbersProvider.notifier).addNumbersInPlace();
            },
          ),
          FloatingActionButton(
            child: const Icon(Icons.access_time_outlined),
            onPressed: () {
              ref.read(timestampProvider.notifier).copy();
            },
          ),
        ],
      ),
    );
  }
}
