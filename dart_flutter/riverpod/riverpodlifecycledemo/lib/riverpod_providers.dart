import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'riverpod_providers.g.dart';

@riverpod
String hello(Ref ref) {
  debugPrint('[HelloProvider] Build');
  return 'Hello World!';
}

@riverpod
String greeting(Ref ref, String otherMessage) {
  debugPrint('[GreetingProvider] Build for $otherMessage');
  return 'Hello World! $otherMessage';
}

@riverpod
class HelloAt extends _$HelloAt {
  @override
  String build(String othersName) {
    debugPrint('[HelloAtProvider] Build for $othersName');
    return _buildGreeting();
  }

  void refresh() {
    debugPrint('[HelloAtProvider] update for $othersName');
    state = _buildGreeting();
    if (othersName == 'Terry') {
      ref.invalidateSelf();
    }
  }

  String _buildGreeting() => 'Hello $othersName! @ ${DateTime.now()})';
}

@riverpod
class SeenNumbers extends _$SeenNumbers {
  @override
  List<num> build() {
    debugPrint('[SeenNumbersProvider] Build');
    return [1, 2, 3];
  }

  void addNumberNewState() {
    debugPrint(
        '[SeenNumbersProvider] adding number new state. Before state = $state');
    final newNum = state.last + 1;
    state = [...state, newNum];
    debugPrint(
        '[SeenNumbersProvider] adding number new state. After state = $state');
  }

  void addNumbersInPlace() {
    debugPrint(
        '[SeenNumbersProvider] adding number in place. Before state = $state');
    final newNum = state.last + 1;
    state.add(newNum);
    ref.notifyListeners();
    debugPrint(
        '[SeenNumbersProvider] adding number in place. After state = $state');
  }
}

@riverpod
class Timestamp extends _$Timestamp {
  @override
  TimestampWrapper build() {
    debugPrint('[TimestampProvider] Build');
    return TimestampWrapper.now();
  }

  @override
  bool updateShouldNotify(TimestampWrapper previous, TimestampWrapper next) {
    //For classes that you can override the == and hashCode method on could
    //do that and return comparison result:
    //  previous != next
    return previous.dateTime != next.dateTime;
  }

  void copy() {
    debugPrint('[TimestampProvider] copy');
    state = state.copy();
  }
}

class TimestampWrapper {
  final DateTime dateTime;

  TimestampWrapper(this.dateTime);

  factory TimestampWrapper.now() => TimestampWrapper(DateTime.now());

  TimestampWrapper copy() => TimestampWrapper(dateTime);
}
