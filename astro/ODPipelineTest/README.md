This is a project to test doing a from measurements to full orbit determination
solutions using [OpenOrb](https://github.com/oorb/oorb).  To run it make sure
you have OpenOrb installed (the default assumes it's installed under Anaconda 
Python 3) and the auxiliary dependencies for `pyplot` to run (should have been
part of the install when Anaconda was run).  This is running command line not
Python wrapped OpenOrb since the OD capabilities weren't available there yet.
To get this running simply CD down into the directory and run:

`python ODPipelineTest.py`  

Again, this assumes Python 3.7. In the top of the `main()` function will be
settings that can be toggled.