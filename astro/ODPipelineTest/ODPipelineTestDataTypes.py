from dataclasses import dataclass
import fortranformat as ff

des_obs_line_format =  \
    ff.FortranRecordWriter("(A,1X,F16.10,1X,A,3(1X,F14.10),2(1X,A),2(1X,F14.10),1X,F14.10,1X,E14.7,1X,A)")


@dataclass
class Observation:
    object_id: str
    observatory_code: str
    epoch_mjd: float
    ra_deg: float
    dec_deg: float
    mag: float = 99.9
    ra_uncertainty_asec: float = 1.0
    dec_uncertainty_asec: float = 1.0
    mag_uncertainty: float = -1.0
    s2n: float = -1.0
    filter_str: str = "X"

    def des_obs_string(self) -> str:
        return des_obs_line_format.write(
            [self.object_id, self.epoch_mjd, "O", self.ra_deg, self.dec_deg, self.mag, self.filter_str,
             self.observatory_code, self.ra_uncertainty_asec, self.dec_uncertainty_asec, self.mag_uncertainty,
             self.s2n, "X"])
