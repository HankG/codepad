import math
import os
import subprocess
import numpy as np
import tempfile
import matplotlib.pyplot as plt

from ODPipelineTestDataTypes import Observation
from pathlib import Path
from datetime import datetime
from datetime import timedelta
from astropy.time import Time
from astroquery.jplhorizons import Horizons
from astroquery.jplhorizons import conf
conf.horizons_server = 'https://ssd.jpl.nasa.gov/horizons_batch.cgi'

#Constants
GM = 132712440041.93938   #From JPL Horizons
AU_Meters = 1.49597870e+11  # Astronomical Units in meters
AU_Km = 149597870.0     # conversion from AU to km
AU_per_day = 1731456.84 # AU/Day converted to Meters per second
rad_hour = math.pi/12.0
rad_min = rad_hour/60.0
rad_sec = rad_min/60.0
rad_deg = math.pi/180.0
rad_amin = rad_deg/60.0
rad_asec = rad_amin/60.0
oorbExecutablePath = "oorb"


def convert_horizon_ephemerides_to_obs(object_id, observatory_code, ephemerides):
    observations = [];
    for e in ephemerides:
        obj_id = object_id
        jd = e["datetime_jd"]
        mjd = jd - 2400000.5
        ra_deg = e["RA"]
        dec_deg = e["DEC"]
        mag = e["V"]
        ra_uncertainty = e["RA_3sigma"]
        dec_uncertainty = e["DEC_3sigma"]
        new_obs = Observation(object_id=obj_id, observatory_code=observatory_code, epoch_mjd=mjd, ra_deg=ra_deg,
                              dec_deg=dec_deg, mag=mag, ra_uncertainty_asec=ra_uncertainty,
                              dec_uncertainty_asec=dec_uncertainty)
        observations.append(new_obs)
    return observations


def get_ephemeris(obj_id, start_time, stop_time, step_string):
    obj = Horizons(id=str(obj_id), location=None,
                   epochs={'start': start_time, 'stop': stop_time, 'step': step_string})
    vec = obj.vectors()  # state vector at each epoch, inital state vector is initial epoch
    vec_array = np.array(vec);

    ephem = []
    for x in range(0,vec_array.size):
        JPL_SV = np.array([vec[i][x] for i in vec.columns])
        # JPL r and v vectors
        JPL_r = np.array([np.float(i) for i in JPL_SV[5:8]]) * AU_Km  # 1.496e+8
        JPL_v = np.array([np.float(i) for i in JPL_SV[8:11]]) * (AU_per_day / 1000)  # KM per second  1731.46
        new_ephem = np.zeros(7)
        new_ephem[0] = np.float(JPL_SV[1]) - 2400000.5
        new_ephem[1] = JPL_r[0]
        new_ephem[2] = JPL_r[1]
        new_ephem[3] = JPL_r[2]
        new_ephem[4] = JPL_v[0]
        new_ephem[5] = JPL_v[1]
        new_ephem[6] = JPL_v[2]
        ephem.append(new_ephem);
    return ephem


def get_oorb_ephemeris(orb_file_path):
    ephemeris = []
    with open(orb_file_path,"r") as orb_file:
        for l in orb_file.readlines():
            line = l.strip()
            if line.startswith("#"):
                continue
            values = line.split()
            new_ephem = np.zeros(7)
            new_ephem[0] = np.float(values[7])
            new_ephem[1] = np.float(values[1]) * AU_Km
            new_ephem[2] = np.float(values[2]) * AU_Km
            new_ephem[3] = np.float(values[3]) * AU_Km
            new_ephem[4] = np.float(values[4]) * (AU_per_day / 1000)
            new_ephem[5] = np.float(values[5]) * (AU_per_day / 1000)
            new_ephem[6] = np.float(values[6]) * (AU_per_day / 1000)
            ephemeris.append(new_ephem)
    return ephemeris


def perform_oorb_iod(configFile, obsInFile, orbOutFile):
    # oorb --task=ranging --conf=oorb.conf --obs-in=iod_obs.des --orb-out=iod.orb
    call = [oorbExecutablePath,
             "--task=ranging",
             "--conf={}".format(configFile),
             "--obs-in={}".format(obsInFile),
             "--orb-out={}".format(orbOutFile)]
    subprocess.call(call)


def perform_oorb_od(configFile, obsInFile, orbInFile, orbOutFile):
    # oorb --task=lsl --conf=oorb.conf --obs-in=od_obs.des --orb-in=iod.orb --orb-out=full-od.orb
    call = [oorbExecutablePath,
             "--task=lsl",
             "--conf={}".format(configFile),
             "--obs-in={}".format(obsInFile),
             "--orb-in={}".format(orbInFile),
             "--orb-out={}".format(orbOutFile)]
    subprocess.call(call)


def perform_oorb_propagation(configFile, orbInFile, initialEpochMJD, outputStepSizeDays, orbOutFile):
    # oorb --task=propagation --conf=oorb.conf --orb-in=full-orbit.orb --epoch-mjd-tt=54648.0 --output-interval-days=1.0 --orb-out=/tmp/fullout.orb
    call = [oorbExecutablePath,
             "--task=propagation",
             "--conf={}".format(configFile),
             "--orb-in={}".format(orbInFile),
             "--epoch-mjd-tt={}".format(initialEpochMJD),
             "--output-interval-days={}".format(outputStepSizeDays),
             "--orb-out={}".format(orbOutFile)]
    subprocess.call(call)


def read_horizons_ephemerides(obj_id, location_id, start_time, stop_time, step_string):
    obj = Horizons(id=str(obj_id), location=str(location_id),
                   epochs={'start': start_time.isoformat(), 'stop': stop_time.isoformat(), 'step': step_string})
    ephemerides = obj.ephemerides()
    return ephemerides


def read_fake_observations():
    observations = []
    obj_id = "OBJ1   "
    observatory_code = "  S1"
    for i in range (0, 10):
        mjd = 50000 + i
        ra_deg = i * 10
        dec_deg = i
        mag = i / 10.0;
        new_obs = Observation(object_id=obj_id, observatory_code=observatory_code, epoch_mjd=mjd, ra_deg=ra_deg,
                              dec_deg=dec_deg, mag=mag)
        observations.append(new_obs)
    return observations


def write_des_obs_file(obs_file_path, observations):
    with open(obs_file_path,"w") as obs_file:
        for o in observations:
            obs_file.write(o.des_obs_string())
            obs_file.write("\n")


def setupOorbEnv(oorbDirectory, \
				 oorb_data="", \
				 oorb_gnuplot=""):
	print(f"Setting OORB_DATA to {oorb_data}")
	if not oorb_data:
		oorb_data = os.path.join(os.path.abspath(oorbDirectory), "data")
	os.environ["OORB_DATA"] = oorb_data
	print(f"Setting GNUPLOT scripts dir to {oorb_gnuplot}")
	if not oorb_gnuplot:
		oorb_gnuplot = os.path.join(os.path.abspath(oorbDirectory), "gnuplot")
	os.environ["OORB_GNUPLOTS_SCRIPTS_DIR"] = oorb_gnuplot
	return


def main():

    # Directory information for OORB setup
    oorbConfigFile = "./oorb.conf"

	# Run settings for this execution
	# Set a fixed location
    # baseDir = "/tmp/oorb-test"
    # Generate a new tempdir long enough for calculations
    tmpDir = tempfile.TemporaryDirectory(prefix="oorb-od")
    baseDir = tmpDir.name
    
    # Horizons database ID
    real_object_id = "433" # "433    " = Eros, "25143  " = Itokawa, "K08K42V" "A899 NG" "Vesta  " "467372"
    
    # Horizons station ID (Make sure the string is exactly 4 characters)
    station_id = " 568" 
    
    # Start time of analysis  
    analysis_start_time = datetime(2008, 5, 31)
    
    # IOD Settings
    iod_duration_days = 0.25
    iod_start_time = analysis_start_time
    iod_dt = timedelta(days=iod_duration_days)
    iod_stop_time = iod_start_time + iod_dt
    iod_step = "1h" 

    
    # Sampling for full OD run and step size within for observation sampling
    od_sampling_duration_days = 60
    od_step_size="7d"
    
    # Propagation settings from OD epoch forward, and steps sizes
    propagation_time_period_days = 60
    propagation_step_size_days = 1 # must be integer
    propagation_step_size_str=f"{propagation_step_size_days}d"

    
    # Processing file locations
    iod_obs_file_path = os.path.join(baseDir,"iod_obs.des")
    iod_orbit_solution = os.path.join(baseDir,"iod.orb")
    od_obs_file_path = os.path.join(baseDir,"od_obs.des")
    od_orbit_solution = os.path.join(baseDir,"full-od.orb")
    propagated_orbits = os.path.join(baseDir,"propagated.orb")
    oorb_object_id = "AAAAAAA"


    print("Setup OpenOrb")
    oorb_root = os.path.join(str(Path.home()),"anaconda3","bin")
    oorb_data_dir = os.path.join(str(Path.home()),"anaconda3","share","openorb")
    oorb_gnuplot_dir = os.path.join(str(os.getcwd()),"gnuplot")
    setupOorbEnv(oorb_root, oorb_data_dir, oorb_gnuplot_dir)

    print("OD Pipeline Test")
    print("Pull IOD observations from Horizons")
    iod_ephemerides = read_horizons_ephemerides(real_object_id, station_id, iod_start_time, iod_stop_time, iod_step)
    iod_observations = convert_horizon_ephemerides_to_obs(oorb_object_id, station_id, iod_ephemerides)
    write_des_obs_file(iod_obs_file_path, iod_observations)

    print("Perform IOD")
    perform_oorb_iod(oorbConfigFile,iod_obs_file_path, iod_orbit_solution)

    print("Pull OD observations from Horizons")
    od_time = timedelta(days=od_sampling_duration_days)
    od_stop_time = analysis_start_time + od_time

    od_ephemerides = read_horizons_ephemerides(real_object_id, station_id, analysis_start_time, od_stop_time, od_step_size)
    od_observations = convert_horizon_ephemerides_to_obs(oorb_object_id, station_id, od_ephemerides)
    write_des_obs_file(od_obs_file_path, od_observations)

    print("Perform OD")
    perform_oorb_od(oorbConfigFile, od_obs_file_path, iod_orbit_solution, od_orbit_solution)

    print("Perform OpenOrb Propagation")
    # figure out new MJD
    ephemeris_end = od_stop_time + timedelta(days=propagation_time_period_days)
    ephemeris_end_dts = Time([ephemeris_end.isoformat()], format="isot", scale="utc")
    ephemeris_end_mjd = ephemeris_end_dts[0].mjd
    perform_oorb_propagation(oorbConfigFile, od_orbit_solution, ephemeris_end_mjd,
                             propagation_step_size_days, propagated_orbits)

    print("Read back in OpenOrb Propagated Ephemeris")
    oorb_ephemeris = get_oorb_ephemeris(propagated_orbits)
    print("Calculate Ephemeris Span for seeding Horizons pull")

    compare_ephemeris_start = Time(oorb_ephemeris[0][0], format="mjd").isot
    compare_ephemeris_stop = Time(oorb_ephemeris[len(oorb_ephemeris)-1][0], format="mjd").isot

    print("Pull ephemeris for object from Horizons")
    horizon_ephem = get_ephemeris(real_object_id, compare_ephemeris_start,
                                  compare_ephemeris_stop, propagation_step_size_str)

    print("Perform consistency analysis")
    times = []
    x_deltas = []
    y_deltas = []
    z_deltas = []
    vx_deltas = []
    vy_deltas = []
    vz_deltas = []
    length = len(oorb_ephemeris)
    if len(horizon_ephem) != length:
        print("Horizons ephemeris length doesn't match oorb, using shortest")
        if len(horizon_ephem) < length:
            length = horizon_ephem
    for i in range(0, length):
        dt = oorb_ephemeris[i][0] - horizon_ephem[i][0]
        if dt > 0.0001:
            print("Times are not consistent for {i} point")
        times.append(oorb_ephemeris[i][0])
        x_deltas.append(oorb_ephemeris[i][1] - horizon_ephem[i][1])
        y_deltas.append(oorb_ephemeris[i][2] - horizon_ephem[i][2])
        z_deltas.append(oorb_ephemeris[i][3] - horizon_ephem[i][3])
        vx_deltas.append(oorb_ephemeris[i][4] - horizon_ephem[i][4])
        vy_deltas.append(oorb_ephemeris[i][5] - horizon_ephem[i][5])
        vz_deltas.append(oorb_ephemeris[i][6] - horizon_ephem[i][6])

    plt.plot(times,x_deltas,"rs", times, y_deltas, "gs", times, z_deltas, "bs")
    plt.ylabel("Position Errors (km)")
    plt.show()
    plt.plot(times, vx_deltas, "rs", times, vy_deltas, "gs", times, vz_deltas, "bs")
    plt.ylabel("Velocity Errors (km/s)")
    plt.show()
    
main()
