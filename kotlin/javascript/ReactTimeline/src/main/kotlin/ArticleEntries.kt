import kotlinx.css.Color
import kotlin.js.Date

data class Article(
    val text:String,
    val date:Date,
    val category: Category,
    val link:Link)

data class Category(
    val tag:String,
    val color:Color,
)
data class Link(
    val url:String,
    val text:String)

enum class Categories(val category:Category) {
    Linux(Category("Linux", Color.blueViolet)),
    Mac(Category("Mac", Color.gold))
}

fun DefaultArticles():List<Article> = listOf(
    Article(
        text = "Rocky Linux 1st release",
        date = Date(2020, 12, 24),
        category = Categories.Linux.category,
        link = Link("https://www.zdnet.com/article/rocky-linux-first-release-is-coming-in-q2-2021-say-developers/", "Rocky Mountain Linux Link")
    ),
    Article(
        text = "Will your laptop run Big Sur?",
        date = Date(2020, 12, 22),
        category = Categories.Mac.category,
        link = Link("https://www.msn.com/en-us/news/technology/macos-big-sur-compatibility-will-your-laptop-work-with-the-new-os/ar-BB15QrZ2?stream=world", "MSN link")
    )
)
