import kotlinx.css.*
import kotlinx.css.properties.*
import styled.StyleSheet

object TimelineStyles: StyleSheet("TimelineStyles", isStatic = true)  {
    val timelineContainer by css{
        fontFamily = "Arial, Helvetica, sans-serif"
        lineHeight = LineHeight("1.5")
        display = Display.flex
        flexDirection = FlexDirection.column
        position = Position.relative
        margin(40.px, 0.px)
        after {
            backgroundColor = Color("#e17b77")
            content = QuotedString("")
            position = Position.absolute
            left = (50.pct - 2.px)
            width = 4.px
            height = 100.pct
        }
    }

    val timelineItem by css {
        display = Display.flex
        justifyContent = JustifyContent.flexEnd
        paddingRight = 30.px
        position = Position.relative
        margin(10.px, 0.px)
        width = 50.pct
        nthChild("odd") {
            alignSelf = Align.flexEnd
            justifyContent = JustifyContent.flexStart
            paddingLeft = 30.px
            paddingRight = 0.px
        }
    }

    val timelineItemContent by css {
        boxShadow(color = rgba(0, 0, 0, 0.3), blurRadius=5.px)
        borderRadius = 4.px
        backgroundColor = Color.white
        display = Display.flex
        flexDirection = FlexDirection.column
        alignItems = Align.flexEnd
        padding(15.px)
        position = Position.relative
        width = 400.px
        maxWidth = 70.pct
        textAlign = TextAlign.right

        after {
            content = QuotedString(" ")
            backgroundColor = Color("#fff")
            boxShadow(color=rgba(0,0,0,0.2), offsetX = 1.px, offsetY = (-1).px, blurRadius = 1.px)
            position = Position.absolute
            right = (-7.5).px
            top = (50.pct - 7.5.px)
            transform { rotate(45.deg) }
            width = 15.px
            height = 15.px
        }
        nthChild("odd") {
            textAlign = TextAlign.left
            alignItems = Align.flexStart
            after {
                right = LinearDimension.auto
                left = (-7.5).px
                boxShadow(color=rgba(0,0,0,0.2), offsetX = (-1).px, offsetY = 1.px, blurRadius = 1.px)
            }
        }

        time {
            color = Color("#777")
            fontSize = 12.px
            fontWeight = FontWeight.bold
        }
        p {
            fontSize = 16.px
            lineHeight = LineHeight("24.px")
            margin(15.px, 0.px)
            maxWidth = 250.px
        }
        a {
            fontSize = 14.px
            fontWeight = FontWeight.bold
            after {
                content = QuotedString(" ►")
                fontSize = 12.px
            }
        }
    }

    val tag by css {
        color = Color.white
//        fontSize = 12.px
//        fontWeight = FontWeight.bold
//        top = 5.px
//        left = 5.px
//        letterSpacing = 1.px
//        padding(5.px)
//        position = Position.absolute
//        textTransform = TextTransform.uppercase
//        nthChild("odd") {
//            left = LinearDimension.auto
//            right = 5.px
//        }
//        nthChild("even") {
//            left = 5.px
//        }
    }

    val circle by css(timelineItem) {
        backgroundColor = Color.white
        border(3.px, BorderStyle.solid, Color("#e17b77"))
        borderRadius = 50.pct
        position = Position.absolute
        top = 50.pct - 10.px
        right = (-40).px
        width = 20.px
        height = 20.px
        zIndex = 100
        nthChild("odd") {
            right = LinearDimension.auto
            left = (-40.px)
        }
    }
}