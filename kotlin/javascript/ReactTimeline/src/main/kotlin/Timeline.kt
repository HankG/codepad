import kotlinx.css.Color
import kotlinx.css.backgroundColor
import kotlinx.css.color
import react.*
import react.dom.*
import styled.css
import styled.styledDiv
import styled.styledSpan
import styled.styledTime

external interface TimelineProps: RProps {
    var articles: List<Article>
}
class Timeline: RComponent<TimelineProps, RState>() {
    override fun RBuilder.render() {
        div("timeline-container") {
            props.articles.forEachIndexed { idx, curArticle ->
                timelineItem {
                    article = curArticle
                    index = idx
                }
            }
        }
    }
}