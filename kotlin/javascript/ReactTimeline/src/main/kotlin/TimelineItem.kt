import kotlinx.css.backgroundColor
import react.*
import react.dom.*
import styled.css
import styled.styledSpan

external interface TimelineItemProps: RProps {
    var article: Article
    var index:Int
}
class TimelineItem: RComponent<TimelineItemProps, RState>() {
    override fun RBuilder.render() {
        div("timeline-item") {
            div("timeline-item-content") {
                key = "${props.index}"
                styledSpan {
                    css {
                        +"tag"
                        backgroundColor = props.article.category.color
                    }
                    +"${props.article.category.tag}"
                }

                time { +"${props.article.date.toDateString()}" }
                p { +props.article.text}
                a(href=props.article.link.url){+props.article.link.text}
                span("circle") {}
            }
        }
    }
}

fun RBuilder.timelineItem(handler: TimelineItemProps.() -> Unit): ReactElement {
    return child(TimelineItem::class) {
        this.attrs(handler)
    }
}