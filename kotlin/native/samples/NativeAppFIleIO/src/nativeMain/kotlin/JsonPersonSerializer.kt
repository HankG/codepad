import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

class JsonPersonSerializer : PersonSerializer {
    override fun decode(text: String): List<Person> {
        return Json.decodeFromString(text)
    }

    override fun encode(people: List<Person>): String {
        return Json{prettyPrint = true}.encodeToString(people)
    }
}