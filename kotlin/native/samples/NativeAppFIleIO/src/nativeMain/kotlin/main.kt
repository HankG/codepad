import kotlinx.cli.ArgParser
import kotlinx.cli.ArgType


fun main(args:Array<String>) {
    println("Native File I/O Example")
    val argParser = ArgParser("nativeio")
    val filePathArg = argParser.argument(ArgType.String, fullName = "file", description = "File Path")
    argParser.parse(args)
    val filePath = filePathArg.value.trim()

    val textFileProcessor = TextFileProcessor()
    val personSerializer = if(filePath.endsWith("json", ignoreCase = true))
        JsonPersonSerializer()
    else if (filePath.endsWith("csv", ignoreCase = true))
        CsvPersonSerializer()
    else
        error("Unknown file extension")

    val text = textFileProcessor.readAllText(filePath)
    val people = personSerializer.decode(text)
    println("Original People: ")
    people.forEach { println(it) }
    println()

    val newFilePath = "$filePath.1"
    val updatedPeople = people + Person("Jimmy","Simms",19)
    println("Updated People:")
    val updatedPeopleText = personSerializer.encode(updatedPeople)
    println(updatedPeopleText)
    textFileProcessor.writeAllText(newFilePath, updatedPeopleText)
    println()
    println("Updated text written to: $newFilePath")
    println("Finished")
}