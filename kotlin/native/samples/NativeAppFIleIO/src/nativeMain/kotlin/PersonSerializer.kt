interface PersonSerializer {
    fun decode(text:String): List<Person>

    fun encode(people:List<Person>): String
}