/**
 * Class encapsulating CSV parser for Person objects
 *
 * Example Text
 * John,Smith,32
 * Nancy,Jones,75
 */
class CsvPersonSerializer : PersonSerializer{
    override fun decode(text: String): List<Person> {
        val defaultName = ""
        val defaultAge = Int.MIN_VALUE
        return text
            .split("\n")
            .map { line ->
                val values = line.trim().split(",")
                val firstName = if (values.isNotEmpty()) values[0] else defaultName
                val lastName = if (values.size >= 2) values[1] else defaultName
                val age = if (values.size >= 3) values[2].toInt() else defaultAge
                Person(firstName, lastName, age)
            }
            .filter{ it.firstName != defaultName && it.lastName != defaultName && it.age != defaultAge}
    }

    override fun encode(people: List<Person>): String {
        return people
            .map {
                "${it.firstName},${it.lastName},${it.age}"
            }
            .joinToString("\n")
    }
}