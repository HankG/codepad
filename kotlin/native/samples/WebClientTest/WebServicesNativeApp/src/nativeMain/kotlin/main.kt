import io.ktor.client.*
import io.ktor.client.engine.curl.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import io.ktor.client.request.*
import io.ktor.util.*
import kotlinx.coroutines.runBlocking

@KtorExperimentalAPI
fun main() {
    println("SpaceX Launches")
    val LAUNCHES_ENDPOINT = "https://api.spacexdata.com/v3/launches"
    val httpClient = HttpClient(Curl) {
        install(JsonFeature) {
            val json = kotlinx.serialization.json.Json { ignoreUnknownKeys = true }
            serializer = KotlinxSerializer(json)
        }
    }

    println("Built Client")

    runBlocking {
        val launches = httpClient.get(LAUNCHES_ENDPOINT) as List<RocketLaunch>
        println("Requested Launches")
        launches.forEach { println(it) }
    }

    println("DONE")
}