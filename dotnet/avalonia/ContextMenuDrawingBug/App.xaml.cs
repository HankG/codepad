using Avalonia;
using Avalonia.Markup.Xaml;

namespace ContextMenuDrawingBug
{
    public class App : Application
    {
        public override void Initialize()
        {
            AvaloniaXamlLoader.Load(this);
        }
   }
}