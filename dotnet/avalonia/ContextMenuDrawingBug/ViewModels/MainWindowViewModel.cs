﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;

namespace ContextMenuDrawingBug.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        public ObservableCollection<ItemsViewModel> Items { get; }

        public MainWindowViewModel()
        {
            var items = Enumerable.Range(0, 100)
                .Select(i =>
                    new ItemsViewModel(true, DateTime.Today.AddDays(i).ToString(CultureInfo.InvariantCulture)))
                .ToList();
            
            items.Insert(5, new ItemsViewModel(false, "This one has no menu"));
            Items = new ObservableCollection<ItemsViewModel>(items);
        }
    }
}
