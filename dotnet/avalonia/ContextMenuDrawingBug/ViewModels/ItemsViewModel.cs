namespace ContextMenuDrawingBug.ViewModels
{
    public class ItemsViewModel : ViewModelBase
    {
        
        public bool HasMenu { get;  }
        
        public string Text { get; }
        
        

        public ItemsViewModel():this(false, "<No Text>")
        {
            
        }

        public ItemsViewModel(bool hasMenu, string text)
        {
            HasMenu = hasMenu;
            Text = text;
        }
    }
}