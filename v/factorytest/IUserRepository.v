module main

interface IUserRepository {
	init_repo()
	add_user(user User)
	get_users() []User
	get_users_by_age(min_age int, max_age int) []User
	get_info() string
}
