module main

import rand
import json
import os
import vweb

struct WebApp {
	vweb.Context
	id       string = rand.uuid_v4()
mut:
	pretty_json bool = true
	app_repos AppRepositories = AppRepositories {}
}

fn (mut w WebApp) init_server() {
	println(os.environ()['PRETTY_PRINT'])
	pretty := os.environ()['PRETTY_PRINT'].to_lower().bool()
	println("Pretty init? $pretty")
	w.pretty_json = pretty
	println('init_server ${w.id}')
}

['/users']
pub fn (mut w WebApp) get_users() vweb.Result {
	// pretty := w.query['pretty'].to_lower().bool()
	// println('Pretty = $pretty')
	println('get_users ${w.id}')
	users := w.app_repos.get_user_repo().get_users()
	json_data := w.to_json(users)
	return w.json(json_data)
}

fn (w &WebApp) to_json(users []User) string {
	if w.pretty_json {
		return json.encode_pretty(users)
	}

	return json.encode(users)
}
