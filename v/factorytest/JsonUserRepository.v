module main

import json
import os
import rand

struct JsonUserRepository {
	filename string
	id       string = rand.uuid_v4()
mut:
	users []User = []User{}
}

fn (mut r JsonUserRepository) init_repo() {
	if !os.exists(r.filename) {
		eprintln('JSON Repo file does not exist so skipping: $r.filename')
		return
	}

	json_string := os.read_file(r.filename) or { panic('Unable to read file $r.filename') }
	stored_users := json.decode([]User, json_string) or {
		panic('Unable to parse json from $r.filename')
	}
	r.users << stored_users
}

fn (mut r JsonUserRepository) add_user(user User) {
	r.users << user
	json := json.encode(r.users)
	os.write_file(r.filename, json) or { panic('Unable to write to: $r.filename') }
}

fn (mut r JsonUserRepository) get_users() []User {
	return r.users
}

fn (mut r JsonUserRepository) get_users_by_age(min_age int, max_age int) []User {
	return r.users.filter(it.age >= min_age && it.age <= max_age )
}

fn (r JsonUserRepository) get_info() string {
	return 'JSON Repository, instance ID:$r.id'
}
