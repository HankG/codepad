module main

struct NoUserRepo {}

fn (mut r NoUserRepo) init_repo() {
	panic('init_repo not implemented')
}

fn (mut f NoUserRepo) add_user(user User) {
	panic('add_user not implemented')
}

fn (mut f NoUserRepo) get_users() []User {
	panic('get_users not implemented')
}

fn (mut r NoUserRepo) get_users_by_age(min_age int, max_age int) []User {
	panic('get_users(min_age, max_age) not implemented')
}

fn (f NoUserRepo) get_info() string {
	return "NoUserRepo class, shouldn't be accessible"
}
