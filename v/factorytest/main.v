module main

import rand
import vweb

// struct App {
// 	vweb.Context
// 	id string = rand.uuid_v4()
// }

// fn main() {
// 	println('main')
// 	mut app := App{}
// 	vweb.run(&app, 8081)
// }

// ['/id']
// fn (mut a App) get_id() vweb.Result {
// 	println('get_id ${a.id}')
// 	return a.text(a.id)
// }

fn main() {
	mut web_app := WebApp{}
	web_app.init_server()
	println('main ${web_app.id}')
	vweb.run(web_app, 8081)
}
