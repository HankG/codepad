module main

import rand
import sqlite

struct SqliteUserRepository {
	filename string
	id       string = rand.uuid_v4()
mut: 
	db       sqlite.DB
}

fn (mut r SqliteUserRepository) init_repo() {
	r.db = sqlite.connect(r.filename) or { panic(err) }
	sql r.db {
		create table User
	}
}

fn (mut r SqliteUserRepository) add_user(user User) {
	sql r.db {
		insert user into User
	}
}

fn (mut r SqliteUserRepository) get_users() []User {
	return sql r.db {
		select from User
	}
}

fn (mut r SqliteUserRepository) get_users_by_age(min_age int, max_age int) []User {
	return sql r.db {
		select from User where age >= min_age && age <= max_age
	}
}

fn (r SqliteUserRepository) get_info() string {
	return 'Sqlite User Repository, instance ID:$r.id'
}