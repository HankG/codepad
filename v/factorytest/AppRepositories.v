module main

import os

struct AppRepositories {
mut:
	user_repo IUserRepository = NoUserRepo{}
}

fn (mut a AppRepositories) get_user_repo() IUserRepository {
	if !(a.user_repo is NoUserRepo) {
		return a.user_repo
	}

	repo_type := if os.args.len < 2 { 'json' } else { os.args[1] }
	if repo_type == 'json' {
		filename := if os.args.len < 3 { '/tmp/test.json' } else { os.args[2] }
		mut json_repo := JsonUserRepository{
			filename: filename
		}
		a.user_repo = json_repo 
	} else if repo_type == 'sqlite' {
		filename := if os.args.len < 3 { '/tmp/test_sqlite.db' } else { os.args[2] }
		mut json_repo := SqliteUserRepository{
			filename: filename
		}
		a.user_repo = json_repo 		
	} else {
		mut mem_repo := MemoryUserRepository{}
		a.user_repo = mem_repo
	}
	a.user_repo.init_repo()

	return a.user_repo
}