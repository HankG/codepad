module main

import rand

struct MemoryUserRepository {
	id string = rand.uuid_v4()
mut:
	users []User = []User{}
}

fn (mut r MemoryUserRepository) init_repo() {
	// nothing to do on initialization
}

fn (mut r MemoryUserRepository) add_user(user User) {
	r.users << user
}

fn (mut r MemoryUserRepository) get_users() []User {
	return r.users
}

fn (mut r MemoryUserRepository) get_users_by_age(min_age int, max_age int) []User {
	return r.users.filter(it.age >= min_age && it.age <= max_age)
}

fn (r MemoryUserRepository) get_info() string {
	return 'InMemory Repository, instance ID:$r.id'
}
